﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Produkty : MonoBehaviour
{
	public List<Produkt> ListaWszystkichProduktow = new List<Produkt>();
	public List<Produkt> ListaProduktowDanegoSklepu = new List<Produkt>();


	void Start()
	{
	}

	public List<Produkt> GetListaWszystkichProduktow()
	{
		return ListaWszystkichProduktow;
	}
	public List<Produkt> GetListaProduktowDanegoSklepu()
	{
		return ListaProduktowDanegoSklepu;
	}
	public void SetListaProduktowDanegoSklepu(List<Produkt> produkts)
	{
		 ListaProduktowDanegoSklepu = produkts;
	}



}


public class Produkt
{
		public Produkt(string NazwaProduktu, float CenaProduktu, int IloscProduktow, Sprite ObrazekProduktu)
		{
		Nazwa = NazwaProduktu;
		Cena = CenaProduktu;
		Obrazek = ObrazekProduktu;
		Ilosc = IloscProduktow;
		}
	public Produkt(string NazwaProduktu, float CenaProduktu, int IloscProduktow)
	{
		Nazwa = NazwaProduktu;
		Cena = CenaProduktu;
		Ilosc = IloscProduktow;
	}

	public string Nazwa;
	public float Cena;
	public int Ilosc;
	public Sprite Obrazek;

}
