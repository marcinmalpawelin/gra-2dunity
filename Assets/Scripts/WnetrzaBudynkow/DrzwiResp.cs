﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrzwiResp : MonoBehaviour
{

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.collider.name == "Player")
		{
			var respawn = FindObjectOfType<Respawn>();
			var resp = respawn.Domek.GetComponentInChildren<RespawnBudynku>();
			Vector3 vector3 = new Vector3(resp.transform.position.x,resp.transform.position.y,resp.transform.position.z);
			collision.transform.SetPositionAndRotation(vector3, new Quaternion());

		}


	}

}
