﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{

	public GameObject Domek { get; set; }
	public Transform PozycjaDomku;

	public void UstawPozycjeDomku(Transform pozycja)
	{
		
		PozycjaDomku = pozycja;
	}

}
