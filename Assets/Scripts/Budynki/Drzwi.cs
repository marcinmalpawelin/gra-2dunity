﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drzwi : MonoBehaviour
{

	public BudynkiControlers BudynkiControlers { get; set; }
	private void OnCollisionEnter2D(Collision2D collision)
	{
		
		Budynek budynek = GetComponentInParent<Budynek>();
		if(budynek.MiejsceTeleportacji ==null)
		{
			BudynkiControlers.PrzeniesGraczaDoNowegoDomku(budynek);
		}
		else
		{
			BudynkiControlers.PrzeniesGraczaDoDomku(budynek);

		}



	}
	// Start is called before the first frame update
	void Start()
    {
		BudynkiControlers = FindObjectOfType<BudynkiControlers>();
    }


}
