﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Budynek : MonoBehaviour
{
	public new Renderer renderer;
	public float WielkoscBudynku;
	public Transform MiejsceTeleportacji { get; set; }
	// Start is called before the first frame update
	void Start()
    {
		renderer = GetComponent<Renderer>();
		WielkoscBudynku = renderer.bounds.extents.magnitude;
    }

}
