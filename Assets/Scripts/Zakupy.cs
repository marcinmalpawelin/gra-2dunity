﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zakupy : MonoBehaviour
{

	public PlayerMove Player;
	public Image[] images;
	public Text[] Ilosc;
	public Text[] Cena;
	string NazwaSprzedawcy;


	private void CzyscListe()
	{
		for(int i = 0; i < images.Length; i++)
		{
			images[i].sprite = null;
			Ilosc[i].text = "";
			Cena[i].text = "";
		}
		NazwaSprzedawcy = null;
	}
	public void AktualizujListe(List<Produkt> produkts, string NazwaSklepu)
	{
		NazwaSprzedawcy = NazwaSklepu;
		CzyscListe();
		Produkt[] produktsTablica = produkts.ToArray();

		for(int i = 0; i <= produktsTablica.Length - 1; i++)
		{
			images[i].sprite = produktsTablica[i].Obrazek;
			Ilosc[i].text = produktsTablica[i].Ilosc.ToString();
			Cena[i].text = produktsTablica[i].Cena.ToString()+"$" ;

		}
	}
	public void Zakup(Produkt produkt)
	{
		if(Player.Kasa>= produkt.Cena)
		{
			Player.Zaplac(float.Parse(Cena.ToString()));

		}
	}


}
