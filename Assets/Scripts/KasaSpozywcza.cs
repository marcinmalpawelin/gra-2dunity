﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UI;

public class KasaSpozywcza : MonoBehaviour
{
	public GameObject Zakupy;
    // Start is called before the first frame update

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.name== "Player")
		{
			Zakupy.SetActive(true);
		}
	}

	private void OnCollisionExit2D(Collision2D collision)
	{
		if(collision.gameObject.name == "Player")
		{
			Zakupy.SetActive(false);
		}
	}
}
