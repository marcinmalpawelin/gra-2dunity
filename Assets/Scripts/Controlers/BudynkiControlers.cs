﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BudynkiControlers : MonoBehaviour
{
	public GameObject DomyslneWnetrzeDomku;
	public GameObject DomyslneWnetrzeSklepuSporzywczego;
	public GameObject Player;
	public GameObject GrydMiasteczka;
	public GameObject Navigatorcontroller;


	public void PrzeniesGraczaDoNowegoDomku(Budynek BudynekWejscia)
	{
		Transform domektrans = DomyslneWnetrzeDomku.transform;
		domektrans.Translate(new Vector3(100f, 100f));
		GameObject NoweWnetrzeDomku;
		switch (BudynekWejscia.tag)
		{	
			case "Sklep Sporzywczy":
				NoweWnetrzeDomku = Instantiate(DomyslneWnetrzeSklepuSporzywczego, domektrans);
				break;

			default:
				NoweWnetrzeDomku = Instantiate(DomyslneWnetrzeDomku, domektrans);
				break;
		}
		
		Respawn resp = NoweWnetrzeDomku.GetComponentInChildren<Respawn>();
		resp.Domek = BudynekWejscia.gameObject;
		resp.UstawPozycjeDomku(BudynekWejscia.transform);
		BudynekWejscia.MiejsceTeleportacji = resp.transform;
		Player.transform.SetPositionAndRotation(new Vector3(resp.transform.position.x, resp.transform.position.y, resp.transform.position.z), new Quaternion());
		GrydMiasteczka.SetActive(false);
		
		Navigation2DMOje navi = Navigatorcontroller.GetComponent<Navigation2DMOje>();
		navi.BakeNavMesh2DAsynch(); // Metoda asynchroniczna do tworzenia mapki po ktrzej mozna puscie agenta


	}
	public void PrzeniesGraczaDoDomku(Budynek budynek)
	{
		Player.transform.SetPositionAndRotation(new Vector3(budynek.MiejsceTeleportacji.transform.position.x, budynek.MiejsceTeleportacji.transform.position.y, budynek.MiejsceTeleportacji.transform.position.z), new Quaternion());

	}

}
