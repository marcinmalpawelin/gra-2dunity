﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigatorControler : MonoBehaviour
{
    public void OdswierzMapkeNawigacji()
	{
		Navigation2DMOje navigation2D2 = GetComponent<Navigation2DMOje>();
		navigation2D2.BakeNavMesh2D();
	}
}
