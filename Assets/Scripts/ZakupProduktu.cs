﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class ZakupProduktu : MonoBehaviour
{
	public Zakupy Zakupy;

	public void Zakup()
	{
		Text[] Ilosc = GetComponentsInChildren<Text>();
		Image Obraz = GetComponent<Image>();
		float Cena = float.Parse(Ilosc[1].text.Trim('$'));
		string ObrazName = Obraz.sprite.name;
		Produkt produkt = new Produkt(ObrazName, Cena, Convert.ToInt32(Ilosc[0].text));

	}
}
