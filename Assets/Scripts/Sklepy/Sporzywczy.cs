﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sporzywczy : MonoBehaviour
{
	public Sprite ChlebIMG;
	public Sprite MąkaIMG;
	public int IloscChleb = 5;
	public int IloscMaka = 0;
	public List<Produkt> ListaProduktowSklepu = new List<Produkt>();
	public GameObject ZakupyUI;

	// Start is called before the first frame update
	void Start()
    {
    }


	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.name == "Player")
		{

			Produkt Chleb = new Produkt("Chleb", 1f, IloscChleb, ChlebIMG);
			Produkt Maka = new Produkt("Mąka", 2f, IloscMaka, MąkaIMG);

			ListaProduktowSklepu.Add(Chleb);
			ListaProduktowSklepu.Add(Maka);


			Zakupy zakupy = ZakupyUI.GetComponent<Zakupy>();
			zakupy.AktualizujListe(ListaProduktowSklepu, this.name);
			ZakupyUI.SetActive(true);
		}
	}


void OnTriggerExit2D(Collider2D col)
	{
		if(col.gameObject.name == "Player")
		{
			ListaProduktowSklepu.Clear();

			ZakupyUI.SetActive(false);
		}
	}


}
