﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PolePrzenicy : MonoBehaviour
{
	public Sprite SpritePustePole;
	public Sprite SpriteZasianePole;
	public Sprite MąkaIMG;
	public List<Produkt> ListaProduktowSklepu = new List<Produkt>();
	public GameObject ZakupyUI;
	float CzasDoWyrosniecia = 10;
	bool CzyPustePole = true;
	public int IloscMaka = 0;

	void Start()
    {
        
    }

    void Update()
    {
		CzasDoWyrosniecia -= Time.deltaTime;
		if(CzasDoWyrosniecia <= 0)
		{
			SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();
			if(CzyPustePole)
			{
				spriteRenderer.sprite = SpriteZasianePole;
				CzyPustePole = false;
			}
			else
			{
				IloscMaka += 10;
				spriteRenderer.sprite = SpritePustePole;

			}
			CzasDoWyrosniecia = 10;
		}
    }

	void OnTriggerEnter2D(Collider2D col)
	{
		Produkt Maka = new Produkt("Mąka", 1f, IloscMaka, MąkaIMG);
		ListaProduktowSklepu.Add(Maka);
		Zakupy zakupy = ZakupyUI.GetComponent<Zakupy>();
		zakupy.AktualizujListe(ListaProduktowSklepu, this.name);
		ZakupyUI.SetActive(true);
	}
	void OnTriggerExit2D(Collider2D col)
	{
		if(col.gameObject.name == "Player")
		{
			ListaProduktowSklepu.Clear();

			ZakupyUI.SetActive(false);
		}
	}
}
