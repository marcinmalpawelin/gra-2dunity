﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{
	Animator Anim;
	public float Predkosc = 1;
	private Rigidbody2D PlayerRigitbody;
	public float Kasa = 1;
	public Text KasaTexta;
	void Start()
	{
		Anim = GetComponent<Animator>();
		PlayerRigitbody = this.GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update()
	{
		
		KasaTexta.text = Kasa.ToString() + "$";
		if (Input.GetKey(KeyCode.W))
		{
			Anim.SetLayerWeight(0, 0f);

			Anim.SetLayerWeight(1,1f);
			PlayerRigitbody.transform.Translate(new Vector2(0, 0.1f * Predkosc));
		}
		if (Input.GetKey(KeyCode.S))
		{
			Anim.SetLayerWeight(0, 0f);

			Anim.SetLayerWeight(1, 1f);

			PlayerRigitbody.transform.Translate(new Vector2(0, -0.1f * Predkosc));
		}

		if (Input.GetKey(KeyCode.A))
		{
			Anim.SetLayerWeight(1, 0f);

			Anim.SetLayerWeight(0, 1f);

			PlayerRigitbody.transform.Translate(new Vector2(-0.1f * Predkosc, 0));
		}
		if (Input.GetKey(KeyCode.D))
		{
			Anim.SetLayerWeight(1, 0f);

			Anim.SetLayerWeight(0, 1f);

			PlayerRigitbody.transform.Translate(new Vector2(0.1f * Predkosc, 0));
		}
		if(Input.GetKey(KeyCode.F))
		{

			Navigation2DMOje navi = GetComponent<Navigation2DMOje>();
			navi.BakeNavMesh2DAsynch(); // Metoda asynchroniczna do tworzenia mapki po ktrzej mozna puscie agenta

		}
		Anim.SetFloat("vertical", Input.GetAxis("Vertical"));
		Anim.SetFloat("horizontal", Input.GetAxis("Horizontal"));
	}
	public void Zaplac(float Cena)
	{
		Kasa -= Cena;
	}
}
