﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettings : MonoBehaviour
{
	private Camera PLayercamera;
    // Start is called before the first frame update
    void Start()
    {
		PLayercamera = this.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
		if(Input.GetAxis("Mouse ScrollWheel") < 0f) // forward
		{
			PLayercamera.orthographicSize +=0.5f;
		}
		else if(Input.GetAxis("Mouse ScrollWheel") > 0f) // backwards
		{
			PLayercamera.orthographicSize -= 0.5f;
		}
	}
}
